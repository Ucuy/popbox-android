package com.lastmile.okihita.lastmile.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lastmile.okihita.lastmile.R;
import com.lastmile.okihita.lastmile.mypopbox.MyPopBoxActivity;
import com.lastmile.okihita.lastmile.o2o.O2OActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainMenuItemAdapter extends RecyclerView.Adapter<MainMenuItemAdapter.ViewHolder> {

    public final static int INDEX_MENU_MYPOPBOX = 1;
    public final static int INDEX_MENU_O2O_SERVICES = 2;
    public final static int INDEX_MENU_PICKUP_REQUEST = 3;
    public final static int INDEX_MENU_POPSHOP = 4;
    public final static int INDEX_MENU_SHOPBYQR = 5;

    private final String TAG = getClass().getSimpleName();
    List<MainMenuItem> mMainMenuItems;

    public MainMenuItemAdapter(List<MainMenuItem> mainMenuItems) {
        mMainMenuItems = mainMenuItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_main_menu_choice, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MainMenuItem mainMenuItem = mMainMenuItems.get(position);
        holder.mTitle.setText(mainMenuItem.getTitle());
    }

    @Override
    public int getItemCount() {
        return mMainMenuItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_MainMenu_TV_title)
        TextView mTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context context = view.getContext();
                    MainMenuItem mainMenuItem = mMainMenuItems.get(getLayoutPosition());
                    switch (mainMenuItem.getId()) {
                        case INDEX_MENU_MYPOPBOX:
                            context.startActivity(new Intent(context, MyPopBoxActivity.class));
                            break;
                        case INDEX_MENU_O2O_SERVICES:
                            context.startActivity(new Intent(context, O2OActivity.class));
                            break;
                    }
                }
            });
        }
    }
}
