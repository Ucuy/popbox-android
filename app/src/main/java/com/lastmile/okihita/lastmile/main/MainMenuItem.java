package com.lastmile.okihita.lastmile.main;

/**
 * Created by okihita on 27-Jun-16.
 */
public class MainMenuItem {
    private String mTitle;
    private int mId;

    public MainMenuItem(int id, String title) {
        mId = id;
        mTitle = title;
    }

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }
}
