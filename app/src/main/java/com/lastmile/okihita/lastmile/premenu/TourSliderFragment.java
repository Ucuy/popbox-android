package com.lastmile.okihita.lastmile.premenu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lastmile.okihita.lastmile.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by okihita on 28-Jun-16.
 */
public class TourSliderFragment extends Fragment {

    @BindView(R.id.tourSlide_bottomText)
    TextView mBottomTextView;
    @BindView(R.id.tourSlide_mainImage)
    ImageView mMainImageView;
    @BindView(R.id.tourSlide_middleText)
    TextView mMiddleTextView;

    public static TourSliderFragment newInstance(int index) {
        TourSliderFragment fragment = new TourSliderFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_tour_slide, container, false);
        ButterKnife.bind(this, view);

        switch (getArguments().getInt("index")) {
            case 1:
                mMiddleTextView.setText("DAILY WORKOUTS");
                mBottomTextView.setText("Daily workouts delivered\nat the touch of your finger!\n" +
                        "Cardio • Strength • Walking •\nYoga • Pilates • Much More!");
                // mMainImageView.setImageResource(R.drawable.tour_img_1);
                break;
            case 2:
                mMiddleTextView.setText("DAILY MEAL PLANS");
                mBottomTextView.setText("Tantalize your taste buds\nwhile losing weight!");
                // mMainImageView.setImageResource(R.drawable.tour_img_2);
                break;
            case 3:
                mMiddleTextView.setText("LOSE WEIGHT\nTHE EASY WAY");
                mBottomTextView.setText("90-days weight-loss program\nto reshape your body,\nand reshape your life!");
                // mMainImageView.setImageResource(R.drawable.tour_img_3);
                break;
            case 4:
                mMiddleTextView.setText("TRAIN YOUR BRAIN");
                mBottomTextView.setText("Inspirational audios to eliminate\nstuck patterns and train your brain!");
                // mMainImageView.setImageResource(R.drawable.tour_img_4);
                break;
        }
        return view;
    }
}
