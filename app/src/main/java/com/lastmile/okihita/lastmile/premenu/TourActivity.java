package com.lastmile.okihita.lastmile.premenu;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import com.lastmile.okihita.lastmile.R;
import com.lastmile.okihita.lastmile.main.MainActivity;
import com.vlonjatg.android.apptourlibrary.AppTour;

public class TourActivity extends AppTour {
    @Override
    public void init(@Nullable Bundle savedInstanceState) {
        int colorOrange = ContextCompat.getColor(this, R.color.deepRed);
        int colorBlue = ContextCompat.getColor(this, R.color.lightRed);

        //Custom slide
        addSlide(TourSliderFragment.newInstance(1), colorOrange);
        addSlide(TourSliderFragment.newInstance(2), colorBlue);
        addSlide(TourSliderFragment.newInstance(3), colorOrange);
        addSlide(TourSliderFragment.newInstance(4), colorBlue);

        //Customize tour
        setInactiveDocsColor(Color.parseColor("#88FFFFFF"));
        setActiveDotColor(Color.WHITE);

        setSkipButtonTextColor(Color.WHITE);
        setSkipText("LOGIN");

        setNextButtonColorToWhite();
        setDoneButtonTextColor(Color.WHITE);
        setDoneText("LOGIN");

    }

    @Override
    public void onSkipPressed() {
        onDonePressed();
    }

    @Override
    public void onDonePressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
