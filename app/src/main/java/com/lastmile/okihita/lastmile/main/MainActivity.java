package com.lastmile.okihita.lastmile.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.lastmile.okihita.lastmile.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();
    @BindView(R.id.main_RV_menuItem)
    RecyclerView mRecyclerView;

    List<MainMenuItem> mMainMenuItems;
    RecyclerView.LayoutManager mLayoutManager;
    MainMenuItemAdapter mMainMenuItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initiateMenuItems();
        setupRecyclerView();
    }

    private void initiateMenuItems() {
        Log.d(TAG, "initiateMenuItems: Initiating menu items...");
        mMainMenuItems = new ArrayList<>();
        mMainMenuItems.add(new MainMenuItem(MainMenuItemAdapter.INDEX_MENU_MYPOPBOX, "My PopBox"));
        mMainMenuItems.add(new MainMenuItem(MainMenuItemAdapter.INDEX_MENU_O2O_SERVICES, "O2O Services"));
        mMainMenuItems.add(new MainMenuItem(MainMenuItemAdapter.INDEX_MENU_PICKUP_REQUEST, "Pickup Request"));
        mMainMenuItems.add(new MainMenuItem(MainMenuItemAdapter.INDEX_MENU_POPSHOP, "PopShop"));
        mMainMenuItems.add(new MainMenuItem(MainMenuItemAdapter.INDEX_MENU_SHOPBYQR, "Shop by QR"));
    }

    private void setupRecyclerView() {
        Log.d(TAG, "setupRecyclerView: ");
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mMainMenuItemAdapter = new MainMenuItemAdapter(mMainMenuItems);
        mRecyclerView.setAdapter(mMainMenuItemAdapter);
    }
}
