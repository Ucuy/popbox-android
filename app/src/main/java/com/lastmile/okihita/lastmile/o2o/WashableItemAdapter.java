package com.lastmile.okihita.lastmile.o2o;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.lastmile.okihita.lastmile.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WashableItemAdapter extends RecyclerView.Adapter<WashableItemAdapter.ViewHolder> {

    List<WashableItem> mWashableItems;

    public WashableItemAdapter(List<WashableItem> washableItems) {
        mWashableItems = washableItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_washable, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WashableItem washableItem = mWashableItems.get(position);

        holder.mTypeTextView.setText(washableItem.getType());
    }

    @Override
    public int getItemCount() {
        return mWashableItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.washable_TV_type)
        TextView mTypeTextView;
        @BindView(R.id.washable_TV_price)
        TextView mPriceTextView;
        @BindView(R.id.washable_TV_qty)
        TextView mQtyTextView;
        @BindView(R.id.washableItem_Button_inc)
        Button mIncButton;
        @BindView(R.id.washableItem_Button_dec)
        Button mDecButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
