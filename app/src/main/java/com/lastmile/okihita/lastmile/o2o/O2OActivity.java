package com.lastmile.okihita.lastmile.o2o;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

import com.lastmile.okihita.lastmile.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class O2OActivity extends AppCompatActivity {

    @BindView(R.id.o2o_CV_laundry)
    CardView mLaundryCardView;
    @BindView(R.id.o2o_CV_phoneService)
    CardView mPhoneServiceCardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o2_o);
        ButterKnife.bind(this);

        mLaundryCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(O2OActivity.this, LaundryServiceActivity.class));
            }
        });
        mPhoneServiceCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(O2OActivity.this, "Coming soon!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
