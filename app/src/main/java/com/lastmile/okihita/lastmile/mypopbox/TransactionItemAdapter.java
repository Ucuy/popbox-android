package com.lastmile.okihita.lastmile.mypopbox;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lastmile.okihita.lastmile.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by okihita on 28-Jun-16.
 */
public class TransactionItemAdapter extends RecyclerView.Adapter<TransactionItemAdapter.ViewHolder> {

    List<TransactionItem> mTransactionItems;

    public TransactionItemAdapter(List<TransactionItem> transactionItems) {
        mTransactionItems = transactionItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transaction, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TransactionItem transactionItem = mTransactionItems.get(position);
        holder.mTypeTextView.setText("TYPE: " + transactionItem.getType());
        holder.mCodeTextView.setText("CODE: " + transactionItem.getCode());
        holder.mDateTextView.setText("DATE: XXX ");
        holder.mStatusTextView.setText("STATUS: " + transactionItem.getStatus());
    }

    @Override
    public int getItemCount() {
        return mTransactionItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.transactionItem_TV_type)
        TextView mTypeTextView;
        @BindView(R.id.transactionItem_TV_code)
        TextView mCodeTextView;
        @BindView(R.id.transactionItem_TV_date)
        TextView mDateTextView;
        @BindView(R.id.transactionItem_TV_status)
        TextView mStatusTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context context = view.getContext();
                    context.startActivity(new Intent(context, TransactionDetail.class));
                }
            });
        }
    }
}
