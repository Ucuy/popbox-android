package com.lastmile.okihita.lastmile.o2o;

/**
 * Created by okihita on 28-Jun-16.
 */
public class WashableItem {

    private int mType;
    private String mPrice;
    private int mQuantity;

    public WashableItem(int type, String price, int quantity) {
        mType = type;
        mPrice = price;
        mQuantity = quantity;
    }

    public int getType() {
        return mType;
    }

    public String getPrice() {
        return mPrice;
    }

    public int getQuantity() {
        return mQuantity;
    }
}
