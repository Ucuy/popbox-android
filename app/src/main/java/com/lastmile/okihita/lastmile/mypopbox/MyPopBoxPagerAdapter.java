package com.lastmile.okihita.lastmile.mypopbox;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class MyPopBoxPagerAdapter extends FragmentPagerAdapter {

    private final String TAG = this.getClass().getSimpleName();
    final int PAGE_COUNT = 2;
    final String[] TAB_TITLES = new String[]{"Transaction", "Profile"};

    /* REMEMBER THAT INDEX STARTS FROM ZERO */
    final int PAGE_INDEX_TRANSACTION = 0;
    final int PAGE_INDEX_PROFILE = 1;

    public MyPopBoxPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "getItem: " + position);
        switch (position) {
            case PAGE_INDEX_TRANSACTION:
                return new TransactionFragment();
            case PAGE_INDEX_PROFILE:
                return new ProfileFragment();
            default:
                Log.d(TAG, "getItem: null");
                return null;
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TAB_TITLES[position];
    }

}
