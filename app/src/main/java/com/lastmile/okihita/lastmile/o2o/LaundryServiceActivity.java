package com.lastmile.okihita.lastmile.o2o;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.lastmile.okihita.lastmile.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LaundryServiceActivity extends AppCompatActivity {

    @BindView(R.id.laundryActivity_RV_washables)
    RecyclerView mWashablesRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laundry_service);
        ButterKnife.bind(this);

    }
}
