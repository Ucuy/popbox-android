package com.lastmile.okihita.lastmile.mypopbox;

import java.util.Date;

/**
 * Created by okihita on 28-Jun-16.
 */
public class TransactionItem {

    public static final int TYPE_POPSHOP = 1;
    public static final int TYPE_O2O_SERVICE = 2;
    public static final int TYPE_PICKUP_REQUEST = 3;

    public static final int STATUS_ORDERED = 1;
    public static final int STATUS_PICKED_UP = 2;
    public static final int STATUS_COMPLETED = 3;

    private int mId;
    private int mType;
    private Date mDate;
    private int mStatus;
    private String mCode;

    public TransactionItem(int id, int type, Date date, int status, String code) {
        mId = id;
        mType = type;
        mDate = date;
        mStatus = status;
        mCode = code;
    }

    public int getId() {
        return mId;
    }

    public int getType() {
        return mType;
    }

    public Date getDate() {
        return mDate;
    }

    public int getStatus() {
        return mStatus;
    }

    public String getCode() {
        return mCode;
    }
}
