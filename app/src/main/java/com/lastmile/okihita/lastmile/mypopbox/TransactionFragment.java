package com.lastmile.okihita.lastmile.mypopbox;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lastmile.okihita.lastmile.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TransactionFragment extends Fragment {

    @BindView(R.id.transaction_RV_transactions)
    RecyclerView mTransactionsRecyclerView;

    RecyclerView.LayoutManager mLayoutManager;
    TransactionItemAdapter mTransactionItemAdapter;
    List<TransactionItem> mTransactionItems;

    public TransactionFragment() {
        // why?
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaction, container, false);
        ButterKnife.bind(this, view);

        initiateTransactionItems();
        setupTransactionRecyclerView();


        return view;
    }

    private void initiateTransactionItems() {
        mTransactionItems = new ArrayList<>();
        mTransactionItems.add(new TransactionItem(1, 1, new Date(), 1, "CODE123"));
        mTransactionItems.add(new TransactionItem(2, 1, new Date(), 1, "CODE234"));
        mTransactionItems.add(new TransactionItem(3, 3, new Date(), 1, "CODEXYZ123"));
        mTransactionItems.add(new TransactionItem(3, 3, new Date(), 1, "CODEXYZ3717"));
        mTransactionItems.add(new TransactionItem(3, 3, new Date(), 1, "CODEX12736721"));
        mTransactionItems.add(new TransactionItem(3, 3, new Date(), 1, "CODEX127123"));
    }

    private void setupTransactionRecyclerView() {
        mTransactionsRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this.getActivity());
        mTransactionsRecyclerView.setLayoutManager(mLayoutManager);

        mTransactionItemAdapter = new TransactionItemAdapter(mTransactionItems);
        mTransactionsRecyclerView.setAdapter(mTransactionItemAdapter);
    }
}
